package com.example.business.repo

import com.example.business.service.CreditReportService
import javax.inject.Inject

interface CreditReportRepository {
    suspend fun fetchCreditReport(): CreditReportDomainModel
}

class CreditReportRepositoryImpl @Inject constructor(
    private val service: CreditReportService
) : CreditReportRepository {
    override suspend fun fetchCreditReport(): CreditReportDomainModel =
        CreditReportDomainModel(service.fetchCreditReport())
}