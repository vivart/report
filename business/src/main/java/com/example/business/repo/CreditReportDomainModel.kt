package com.example.business.repo

import com.example.business.service.CreditReportResponse

data class CreditReportDomainModel(
    val score: Int,
    val maxScore: Int,
    val minScore: Int,
    val description: String
) {
    constructor(response: CreditReportResponse) : this(
        score = response.creditReportInfo.score,
        maxScore = response.creditReportInfo.maxScoreValue,
        minScore = response.creditReportInfo.minScoreValue,
        description = response.creditReportInfo.equifaxScoreBandDescription
    )
}