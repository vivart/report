package com.example.business.di

import com.example.business.CreditReportUseCase
import com.example.business.CreditReportUseCaseImpl
import com.example.business.repo.CreditReportRepository
import com.example.business.repo.CreditReportRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
interface BusinessModule {
    @Binds
    fun CreditReportRepositoryImpl.bindCreditReportRepository(): CreditReportRepository

    @Binds
    fun CreditReportUseCaseImpl.bindCreditReportUseCase(): CreditReportUseCase
}