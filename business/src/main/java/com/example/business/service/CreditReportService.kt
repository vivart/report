package com.example.business.service

import retrofit2.http.GET

interface CreditReportService {

    @GET("endpoint.json")
    suspend fun fetchCreditReport(): CreditReportResponse
}