package com.example.business

import com.example.business.repo.CreditReportDomainModel
import com.example.business.repo.CreditReportRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface CreditReportUseCase {
    suspend fun fetchCreditReportService(): CreditReportDomainModel
}

class CreditReportUseCaseImpl @Inject constructor(
    private val repo: CreditReportRepository
) : CreditReportUseCase {
    override suspend fun fetchCreditReportService() = withContext(Dispatchers.IO) {
        repo.fetchCreditReport()
    }
}