package com.example.business

import com.example.business.service.CreditReportService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CreditReportServiceTest {
    private val server = MockWebServer()
    private lateinit var service: CreditReportService

    companion object {
        private const val PATH = "/endpoint.json";
    }

    @Before
    fun setup() {
        server.start()
        server.dispatcher = setupMockWebServer()
        setupMovieService()
    }

    private fun setupMovieService() {
        service = Retrofit.Builder().baseUrl(server.url("/"))
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(CreditReportService::class.java)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fetchCreditReport should return valid response`() = runBlocking {
        val response = service.fetchCreditReport()
        MatcherAssert.assertThat(response.creditReportInfo.score, equalTo(514))
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    private fun setupMockWebServer(): Dispatcher = object : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            return when (request.path) {
                PATH -> MockResponse()
                    .setResponseCode(200)
                    .setBody(
                        """
                           {
                             "accountIDVStatus": "PASS",
                             "creditReportInfo": {
                                "score": 514,
                                "scoreBand": 4,
                                "clientRef": "CS-SED-655426-708782",
                                "status": "MATCH",
                                "maxScoreValue": 700,
                                "minScoreValue": 0,
                                "monthsSinceLastDefaulted": -1,
                                "hasEverDefaulted": false,
                                "monthsSinceLastDelinquent": 1,
                                "hasEverBeenDelinquent": true,
                                "percentageCreditUsed": 44,
                                "percentageCreditUsedDirectionFlag": 1,
                                "changedScore": 0,
                                "currentShortTermDebt": 13758,
                                "currentShortTermNonPromotionalDebt": 13758,
                                "currentShortTermCreditLimit": 30600,
                                "currentShortTermCreditUtilisation": 44,
                                "changeInShortTermDebt": 549,
                                "currentLongTermDebt": 24682,
                                "currentLongTermNonPromotionalDebt": 24682,
                                "currentLongTermCreditLimit": null,
                                "currentLongTermCreditUtilisation": null,
                                "changeInLongTermDebt": -327,
                                "numPositiveScoreFactors": 9,
                                "numNegativeScoreFactors": 0,
                                "equifaxScoreBand": 4,
                                "equifaxScoreBandDescription": "Excellent",
                                "daysUntilNextReport": 9
                             },
                            "dashboardStatus": "PASS",
                            "personaType": "INEXPERIENCED",
                            "coachingSummary": {
                                "activeTodo": false,
                                "activeChat": true,
                                "numberOfTodoItems": 0,
                                "numberOfCompletedTodoItems": 0,
                                "selected": true
                            },
                        "augmentedCreditScore": null
                        }
                        """.trimIndent()
                    )
                else -> MockResponse().setResponseCode(404)
            }
        }
    }
}