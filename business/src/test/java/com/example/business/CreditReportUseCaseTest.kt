package com.example.business

import com.example.business.repo.CreditReportRepository
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test

class CreditReportUseCaseTest {
    private val repo: CreditReportRepository = mockk(relaxed = true)
    private val useCase = CreditReportUseCaseImpl(repo)

    @Test
    fun `fetchCreditReportService should call fetchCreditReportService on repo`() = runBlocking {
        useCase.fetchCreditReportService()
        coVerify { repo.fetchCreditReport() }
    }
}