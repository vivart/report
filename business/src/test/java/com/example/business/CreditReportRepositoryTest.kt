package com.example.business

import com.example.business.repo.CreditReportDomainModel
import com.example.business.repo.CreditReportRepositoryImpl
import com.example.business.service.CreditReportResponse
import com.example.business.service.CreditReportService
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class CreditReportRepositoryTest {

    private val response: CreditReportResponse = mockk(relaxed = true)
    private val service: CreditReportService = mockk {
        coEvery { fetchCreditReport() } returns response
    }
    private val repo = CreditReportRepositoryImpl(service)

    @ExperimentalCoroutinesApi
    @Test
    fun `fetchCreditReport should fetchCreditReport on service`() = runBlockingTest {
        repo.fetchCreditReport()
        coVerify { service.fetchCreditReport() }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fetchCreditReport should return CreditReportDomainModel`() = runBlockingTest {
        val response = repo.fetchCreditReport()
        assertThat(response, instanceOf(CreditReportDomainModel::class.java))
    }
}