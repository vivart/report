package com.example.report

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.ComposeContentTestRule
import androidx.compose.ui.test.onNodeWithText
import com.example.report.components.CreditReport
import com.example.report.credit.CreditReportUIState

inline fun report(composeTestRule: ComposeContentTestRule, block: ReportRobot.() -> Unit) =
    ReportRobot(composeTestRule).block()

class ReportRobot(private val composeTestRule: ComposeContentTestRule) {
    fun showCreditScoreWith(uiState: CreditReportUIState) {
        composeTestRule.setContent {
            CreditReport(uiState = uiState)
        }
    }
}

inline fun result(composeTestRule: ComposeContentTestRule, block: ReportResult.() -> Unit) =
    ReportResult(composeTestRule).block()

class ReportResult(private val composeTestRule: ComposeContentTestRule) {
    fun verifyTextDisplayed(text: String) {
        composeTestRule.onNodeWithText(text).assertIsDisplayed()
    }
}