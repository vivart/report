package com.example.report

import androidx.compose.ui.test.junit4.createComposeRule
import androidx.test.platform.app.InstrumentationRegistry
import com.example.report.credit.CreditReportModel
import com.example.report.credit.Failure
import com.example.report.credit.Loading
import com.example.report.credit.Success
import org.junit.Rule
import org.junit.Test


class ReportUITest {
    @Rule
    @JvmField
    val composeTestRule = createComposeRule()
    private val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    companion object {
        private const val SCORE = 514
        private const val MAX_SCORE = 700
        private const val MIN_SCORE = 0
        private const val DESCRIPTION = "Excellent"
    }

    @Test
    fun testCreditReportLoading() {
        report(composeTestRule) {
            showCreditScoreWith(uiState = Loading)
        }
        result(composeTestRule) {
            verifyTextDisplayed(appContext.getString(R.string.loading))
        }
    }

    @Test
    fun testCreditReportSuccess() {
        report(composeTestRule) {
            showCreditScoreWith(
                uiState = Success(
                    CreditReportModel(
                        score = SCORE,
                        maxScore = MAX_SCORE,
                        minScore = MIN_SCORE,
                        description = DESCRIPTION
                    )
                )
            )
        }
        result(composeTestRule) {
            verifyTextDisplayed(appContext.getString(R.string.credit_score_title))
            verifyTextDisplayed("$SCORE")
            verifyTextDisplayed(
                appContext.getString(
                    R.string.credit_score_subtitle,
                    MAX_SCORE
                )
            )
        }
    }

    @Test
    fun testCreditReportFailure() {
        report(composeTestRule) {
            showCreditScoreWith(uiState = Failure(appContext.getString(R.string.network_error)))
        }
        result(composeTestRule) {
            verifyTextDisplayed(appContext.getString(R.string.network_error))
        }
    }
}