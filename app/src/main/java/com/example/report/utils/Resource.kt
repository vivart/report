package com.example.report.utils

import android.content.Context
import androidx.annotation.StringRes
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

interface Resource {
    fun getString(@StringRes stringResourceId: Int): String
}

class ResourceImpl @Inject constructor(@ApplicationContext private val context: Context) :
    Resource {
    override fun getString(stringResourceId: Int) = context.getString(stringResourceId);
}