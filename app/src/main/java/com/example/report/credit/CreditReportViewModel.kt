package com.example.report.credit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.business.CreditReportUseCase
import com.example.report.R
import com.example.report.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

sealed interface CreditReportUIState
object Loading : CreditReportUIState
data class Success(val model: CreditReportModel) : CreditReportUIState
data class Failure(val message: String) : CreditReportUIState

@HiltViewModel
class CreditReportViewModel @Inject constructor(
    private val useCase: CreditReportUseCase,
    private val resource: Resource
) : ViewModel() {

    private val _state: MutableStateFlow<CreditReportUIState> = MutableStateFlow(Loading)
    val state: StateFlow<CreditReportUIState> = _state

    /**
     * Fetch credit report from use case and update ui state
     */
    fun fetchCreditReport() {
        viewModelScope.launch {
            try {
                val info = useCase.fetchCreditReportService()
                _state.emit(Success(CreditReportModel(info)))
            } catch (e: Exception) {
                _state.emit(
                    Failure(
                        e.localizedMessage ?: resource.getString(R.string.network_error)
                    )
                )
            }
        }
    }
}