package com.example.report.credit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.report.components.CreditReportScreen
import com.example.report.databinding.FragmentCreditReportBinding
import com.google.android.material.composethemeadapter.MdcTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CreditReportFragment : Fragment() {

    private val viewModel: CreditReportViewModel by viewModels()
    private var _binding: FragmentCreditReportBinding? = null
    private val binding: FragmentCreditReportBinding
        get() = _binding ?: throw IllegalAccessException("_binding should not be null")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCreditReportBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.fetchCreditReport()
        binding.creditReportInfo.setContent {
            MdcTheme {
                CreditReportScreen(viewModel)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
