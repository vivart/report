package com.example.report.credit

import com.example.business.repo.CreditReportDomainModel

data class CreditReportModel(
    val score: Int,
    val maxScore: Int,
    val minScore: Int,
    val description: String
) {
    constructor(domainModel: CreditReportDomainModel) : this(
        score = domainModel.score,
        maxScore = domainModel.maxScore,
        minScore = domainModel.minScore,
        description = domainModel.description
    )
}