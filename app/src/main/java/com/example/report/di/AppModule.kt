package com.example.report.di

import com.example.report.utils.Resource
import com.example.report.utils.ResourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface AppModule {

    @Binds
    fun ResourceImpl.bindCreditReportRepository(): Resource
}