package com.example.report.components

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ProgressIndicatorDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.report.R
import com.example.report.credit.CreditReportModel
import com.example.report.credit.CreditReportUIState
import com.example.report.credit.CreditReportViewModel
import com.example.report.credit.Failure
import com.example.report.credit.Loading
import com.example.report.credit.Success

@Composable
fun CreditReportScreen(creditReportViewModel: CreditReportViewModel = viewModel()) {
    val uiState by creditReportViewModel.state.collectAsState(initial = Loading)
    CreditReport(uiState = uiState)
}

@Composable
fun CreditReport(uiState: CreditReportUIState) {
    when (uiState) {
        is Loading -> Text(text = stringResource(R.string.loading))
        is Success -> CreditScoreIndicator(model = uiState.model)
        is Failure -> Text(text = uiState.message)
    }
}

@Composable
private fun CreditScoreIndicator(model: CreditReportModel) {
    Box(
        modifier = Modifier.padding(dimensionResource(id = R.dimen.large_padding)),
        contentAlignment = Alignment.Center
    ) {
        Indicator(model.score, model.maxScore)
        Info(model.score, model.maxScore)
    }
}

@Composable
private fun Indicator(score: Int, maxScore: Int, modifier: Modifier = Modifier) {
    val density = LocalDensity.current
    val startAngle = 270f
    val progress = score / maxScore.toFloat()
    val animateFloat = remember { Animatable(0f) }
    LaunchedEffect(animateFloat) {
        animateFloat.animateTo(
            targetValue = progress,
            animationSpec = tween(durationMillis = 3000, easing = LinearEasing)
        )
    }
    val sweep = animateFloat.value * 360f
    val highlightedStroke = with(density) {
        Stroke(width = (ProgressIndicatorDefaults.StrokeWidth * 2).toPx(), cap = StrokeCap.Butt)
    }
    val stroke = with(density) {
        Stroke(width = ProgressIndicatorDefaults.StrokeWidth.toPx(), cap = StrokeCap.Butt)
    }
    val diameterOffset = highlightedStroke.width / 2
    val innerArcPadding =
        with(LocalDensity.current) { dimensionResource(id = R.dimen.indicator_padding).toPx() }
    val indicatorColor = colorResource(id = R.color.score_indicator_color)

    Canvas(modifier = modifier.fillMaxSize()) {
        val arcDimen = size.height.coerceAtMost(size.width) - 2 * diameterOffset
        val xOffset = if (size.height > size.width) highlightedStroke.width / 2 else size.width / 3
        val yOffset = if (size.height > size.width) size.height / 4 else diameterOffset
        drawArc(
            color = Color.Gray,
            startAngle = startAngle,
            sweepAngle = 360f,
            useCenter = false,
            topLeft = Offset(xOffset, yOffset),
            size = Size(arcDimen, arcDimen),
            style = stroke
        )
        drawArc(
            color = indicatorColor,
            startAngle = startAngle,
            sweepAngle = sweep,
            useCenter = false,
            topLeft = Offset(xOffset + innerArcPadding / 2, yOffset + innerArcPadding / 2),
            size = Size(arcDimen - innerArcPadding, arcDimen - innerArcPadding),
            style = highlightedStroke
        )
    }
}

@Composable
private fun Info(score: Int, maxScore: Int, modifier: Modifier = Modifier) {
    Column(modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = stringResource(R.string.credit_score_title),
            style = TextStyle(
                fontSize = dimensionResource(id = R.dimen.title_font_size).value.sp,
                fontWeight = FontWeight.Bold
            ),
        )
        Text(
            text = "$score",
            style = TextStyle(
                fontSize = dimensionResource(id = R.dimen.large_font_size).value.sp,
                color = colorResource(id = R.color.score_color)
            ),
        )
        Text(
            text = stringResource(R.string.credit_score_subtitle, maxScore),
            style = TextStyle(
                fontSize = dimensionResource(id = R.dimen.title_font_size).value.sp,
                fontWeight = FontWeight.Bold
            ),
        )
    }
}