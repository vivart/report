package com.example.report

import com.example.business.CreditReportUseCase
import com.example.business.repo.CreditReportDomainModel
import com.example.report.credit.CreditReportModel
import com.example.report.credit.CreditReportViewModel
import com.example.report.credit.Failure
import com.example.report.credit.Loading
import com.example.report.credit.Success
import com.example.report.utils.Resource
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class CreditReportViewModelTest {
    private val domainModel: CreditReportDomainModel = mockk(relaxed = true)
    private val useCase: CreditReportUseCase = mockk(relaxed = true) {
        coEvery { fetchCreditReportService() } returns domainModel
    }
    private val resource: Resource = mockk(relaxed = true)
    private val viewModel = CreditReportViewModel(useCase, resource)
    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `fetchCreditReport should call fetchCreditReportService on useCase`() =
        dispatcher.runBlockingTest {
            viewModel.fetchCreditReport()
            coVerify { useCase.fetchCreditReportService() }
        }

    @Test
    fun `initial state should be loading`() = dispatcher.runBlockingTest {
        assertThat(Loading, equalTo(viewModel.state.value))
    }

    @Test
    fun `fetchCreditReport should return state Failure, when fetchCreditReportService throws exception `() =
        dispatcher.runBlockingTest {
            coEvery { useCase.fetchCreditReportService() } throws Exception("error")
            viewModel.fetchCreditReport()
            assertThat(Failure("error"), equalTo(viewModel.state.value))
        }

    @Test
    fun `fetchCreditReport should return Success state with model, when fetchCreditReportService return domain model`() =
        dispatcher.runBlockingTest {
            val domainModel: CreditReportDomainModel = mockk {
                every { score } returns 514
                every { maxScore } returns 700
                every { minScore } returns 0
                every { description } returns "description"

            }
            coEvery { useCase.fetchCreditReportService() } returns domainModel
            viewModel.fetchCreditReport()
            assertThat(
                Success(
                    CreditReportModel(
                        score = 514,
                        maxScore = 700,
                        minScore = 0,
                        description = "description"
                    )
                ), equalTo(viewModel.state.value)
            )
        }
}