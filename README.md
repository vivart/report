Sample credit score report app. I have tried to use all latest Android tech stack.

1. MVVM Architecture with the following interaction flow

View -> ViewModel -> UseCase -> Repository -> Service

and following data flow 

Model <- DomainModel <- ResponseModel

2. using Hilt for dependency injection
3. Jetpack compose for credit score UI component.
4. Jetpack architecture components e.g. ViewModel with StateFlow, Navigation and view binding.
5. Espresso UI tests using Robot pattern.
6. Unit tests with Mockk 
7. Retrofit with Kotlin coroutines
8. Modularization with Business module.
